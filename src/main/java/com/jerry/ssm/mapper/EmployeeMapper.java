package com.jerry.ssm.mapper;

import com.jerry.ssm.pojo.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ClassName: EmployeeMapper
 * Package: com.jerry.ssm.mapper
 * Description:
 *
 * @Author jerry_jy
 * @Create 2023-02-11 10:07
 * @Version 1.0
 */
public interface EmployeeMapper {

    /**
     * 查询所有员工信息
     * @return
     */
    List<Employee> getAllEmployee();

    /**
     * 根据id查询员工信息
     * @return
     */
    Employee selectEmployeeInfoById(@Param("id") Integer id);
}
